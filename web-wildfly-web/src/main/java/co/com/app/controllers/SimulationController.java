package co.com.app.controllers;

import co.com.app.models.ProcessBlock;
import com.usta.mainmemorymanager.models.FreeMemory;
import com.usta.mainmemorymanager.models.InstantMemory;
import com.usta.mainmemorymanager.models.WaitingProcess;
import com.usta.mainmemorymanager.service.SimulationService;
import com.usta.mainmemorymanager.utils.Color;
import lombok.Data;
import org.primefaces.PrimeFaces;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
@ViewScoped
@Data
public class SimulationController implements Serializable {

    @EJB
    private SimulationService service;

    private List<InstantMemory> instantMemories;

    private InstantMemory currentInstantMemory;

    private int totalMemory;

    private boolean stop;

    private DonutChartModel donutModel;

    @PostConstruct
    public void init() {
        stop = true;
        totalMemory = 4000;
        currentInstantMemory = new InstantMemory(0, 0, new FreeMemory(totalMemory), new WaitingProcess(), new ArrayList<>());
        instantMemories = new ArrayList<>();
        instantMemories = service.startSimulation(20, totalMemory);
        createDonutModel();
    }

    public void createDonutModel() {
        donutModel = new DonutChartModel();
        ChartData data = new ChartData();

        DonutChartDataSet dataSet = new DonutChartDataSet();
        List<String> memoryNamePartitions = currentInstantMemory.getMemoryNamePartitions();
        List<Number> memoryValuePartitions = currentInstantMemory.getMemoryValuePartitions();

        data.setLabels(memoryNamePartitions);
        dataSet.setData(memoryValuePartitions);
        dataSet.setBackgroundColor(currentInstantMemory.getBlocksColors(memoryNamePartitions).stream()
                .map(Color::getRgbaColor).collect(Collectors.toList()));
        data.addChartDataSet(dataSet);

        donutModel.setData(data);
    }

    public int getPercentage() {
        int usedMemory = totalMemory - currentInstantMemory.getFreeMemorySize();
        return Math.round((usedMemory * 100) / totalMemory);
    }

    public List<ProcessBlock> getProcess() {
        return currentInstantMemory.getProcessesRunning().stream().map(process -> new ProcessBlock(process,
                InstantMemory.processColors.get(process.getName()))).collect(Collectors.toList());
    }

    public void updateSystemInfo() {
        Optional<InstantMemory> optionalInstantMemory = getCurrent(1);
        if (optionalInstantMemory.isPresent()) {
            stop = false;
            currentInstantMemory = optionalInstantMemory.get();
            createDonutModel();
        } else {
            stopSimulation();
        }
    }

    public void setFirst() {
        currentInstantMemory = instantMemories.get(0);
        update();
    }

    public void setLast() {
        currentInstantMemory = instantMemories.get(instantMemories.size() - 1);
        update();
    }

    public void next() {
        Optional<InstantMemory> optionalInstantMemory = getCurrent(1);
        optionalInstantMemory.ifPresent(instantMemory -> currentInstantMemory = instantMemory);
        update();
    }

    public void previous() {
        Optional<InstantMemory> optionalInstantMemory = getCurrent(-1);
        optionalInstantMemory.ifPresent(instantMemory -> currentInstantMemory = instantMemory);
        update();
    }

    private void update() {
        stop = true;
        PrimeFaces.current().ajax().update("systemInfo");
        createDonutModel();
    }

    public void stopSimulation() {
        this.stop = true;
    }

    public boolean isLast() {
        return instantMemories.get(instantMemories.size() - 1).getClock() == currentInstantMemory.getClock();
    }

    public boolean isFirst() {
        return currentInstantMemory.getClock() == 0 ||
                instantMemories.get(0).getClock() == currentInstantMemory.getClock();
    }

    private Optional<InstantMemory> getCurrent(int change) {
        return instantMemories.stream()
                .filter(instantMemory -> instantMemory.getClock() == currentInstantMemory.getClock() + change)
                .findFirst();
    }

}
