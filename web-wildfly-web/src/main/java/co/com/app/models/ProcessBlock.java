package co.com.app.models;

import com.usta.mainmemorymanager.models.Process;
import com.usta.mainmemorymanager.utils.Color;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProcessBlock {
    private Process process;
    private Color color;
}
