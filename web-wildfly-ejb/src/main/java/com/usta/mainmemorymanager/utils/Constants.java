package com.usta.mainmemorymanager.utils;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final int OS_SIZE = 100;

    public static final int MAX_PROCESS_TO_GENERATE = 4;

    public static final int MAX_PROCESS_TIME = 15;

    public static final int MIN_PROCESS_TIME = 2;

    public static final int MAX_PROCESS_SIZE = 800;

    public static final int MIN_PROCESS_SIZE = 50;

    public static final String PROCESS_NAME = "Process-%d";

    public static final String OS_NAME = "OS";

    public static final String FREE_BLOCK_NAME = "Free Memory";

    public static Map<Integer, Color> colors;

    static {
        colors = new HashMap<>();
        colors.put(1, new Color("bg-blue-500", "rgb(33, 150, 243)"));
        colors.put(2, new Color("bg-blue-700", "rgb(23, 105, 170)"));
        colors.put(3, new Color("bg-bluegray-200", "rgb(187, 199, 205)"));
        colors.put(4, new Color("bg-bluegray-500", "rgb(96, 125, 139)"));
        colors.put(5, new Color("bg-bluegray-700", "rgb(67, 88, 97)"));
        colors.put(6, new Color("bg-cyan-200", "rgb(145, 226, 237)"));
        colors.put(7, new Color("bg-cyan-500", "rgb(0, 188, 212)"));
        colors.put(8, new Color("bg-cyan-700", "rgb(0, 132, 148)"));
        colors.put(9, new Color("bg-green-200", "rgb(178, 221, 180)"));
        colors.put(10, new Color("bg-green-500", "rgb(76, 175, 80)"));
        colors.put(11, new Color("bg-green-700", "rgb(53, 123, 56)"));
        colors.put(12, new Color("bg-indigo-200", "rgb(172, 180, 223)"));
        colors.put(13, new Color("bg-indigo-500", "rgb(63, 81, 181)"));
        colors.put(14, new Color("bg-indigo-700", "rgb(44, 57, 127)"));
        colors.put(15, new Color("bg-orange-200", "rgb(251, 199, 145)"));
        colors.put(16, new Color("bg-orange-500", "rgb(245, 124, 0)"));
        colors.put(17, new Color("bg-orange-700", "rgb(172, 87, 0)"));
        colors.put(18, new Color("bg-pink-200", "rgb(246, 158, 188)"));
        colors.put(19, new Color("bg-pink-500", "rgb(233, 30, 99)"));
        colors.put(20, new Color("bg-pink-700", "rgb(163, 21, 69)"));
        colors.put(21, new Color("bg-purple-200", "rgb(212, 162, 221)"));
        colors.put(22, new Color("bg-purple-500", "rgb(156, 39, 176)"));
        colors.put(23, new Color("bg-purple-700", "rgb(109, 27, 123)"));
        colors.put(24, new Color("bg-teal-200", "rgb(145, 210, 204)"));
        colors.put(25, new Color("bg-teal-500", "rgb(0, 150, 136)"));
        colors.put(26, new Color("bg-teal-700", "rgb(0, 105, 95)"));
        colors.put(27, new Color("bg-yellow-200", "rgb(253, 228, 165)"));
        colors.put(28, new Color("bg-yellow-500", "rgb(251, 192, 45)"));
        colors.put(29, new Color("bg-yellow-700", "rgb(176, 134, 32)"));
    }

    public static Color OS_COLOR = new Color("bg-blue-200", "rgb(160, 210, 250)");
    public static Color FREE_MEMORY_COLOR = new Color("surface-50", "rgb(250, 250, 250)");

}
