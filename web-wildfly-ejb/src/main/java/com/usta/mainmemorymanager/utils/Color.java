package com.usta.mainmemorymanager.utils;

public class Color {

    private final String primeColor;
    private final String rgbColor;

    public Color(String primeColor, String rgbColor) {
        this.primeColor = primeColor;
        this.rgbColor = rgbColor;
    }

    public String getPrimeColor() {
        return primeColor;
    }

    public String getRgbaColor() {
        return rgbColor;
    }
}
