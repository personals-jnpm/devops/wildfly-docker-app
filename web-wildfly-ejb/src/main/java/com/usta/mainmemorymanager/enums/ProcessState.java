package com.usta.mainmemorymanager.enums;

public enum ProcessState {
    WAITING_FOR,
    RUNNING,
    FINISHED
}
