package com.usta.mainmemorymanager.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.usta.mainmemorymanager.utils.Constants.OS_SIZE;

@Data
public class FreeMemory {

    private List<Block> blocksMemory;

    public FreeMemory(int sizeMemory) {
        blocksMemory = new ArrayList<>();
        blocksMemory.add(new Block(OS_SIZE, sizeMemory));
    }

    public void addFreeBlockMemory(Block block) {
        blocksMemory.add(block);
        updateFreeMemory();
    }

    private void updateFreeMemory() {
        List<Block> blocks = new ArrayList<>();
        blocksMemory = blocksMemory.stream()
                .sorted(Comparator.comparing(Block::getStartLocation)).collect(Collectors.toList());
        Block block = blocksMemory.get(0);
        for (int i = 1; i < blocksMemory.size(); i++) {
            if (block.getEndLocation() == blocksMemory.get(i).getStartLocation()) {
                block.setEndLocation(blocksMemory.get(i).getEndLocation());
            } else {
                blocks.add(block);
                block = blocksMemory.get(i);
            }
        }
        blocks.add(block);
        blocksMemory = blocks;
    }

    public List<Block> getSortedBlocksBySize() {
        List<Block> sortedBlocksByTotalSize = blocksMemory.stream()
                .sorted(Comparator.comparing(Block::getTotalSize)).collect(Collectors.toList());
        Collections.reverse(sortedBlocksByTotalSize);
        return sortedBlocksByTotalSize;
    }

    private List<Block> cloneList(List<Block> list) {
        List<Block> listCloned = new ArrayList<>();
        for (Block block : list) {
            listCloned.add(new Block(block.getStartLocation(), block.getEndLocation()));
        }
        return listCloned;
    }
}
