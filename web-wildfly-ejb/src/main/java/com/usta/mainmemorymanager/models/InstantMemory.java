package com.usta.mainmemorymanager.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.usta.mainmemorymanager.utils.Color;
import com.usta.mainmemorymanager.utils.Constants;
import com.usta.mainmemorymanager.utils.RandomUtil;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class InstantMemory {

    public static Map<String, Color> processColors = new HashMap<>();
    private static List<Color> availableColors = new ArrayList<>(Constants.colors.values());

    private int clock;
    private int numberProcess;
    private int freeMemorySize;
    private FreeMemory freeMemory;
    private WaitingProcess waitingProcess;
    private List<Process> processesRunning;
    private List<Block> memoryBlocks;

    public InstantMemory(int clock, int numberProcess, FreeMemory freeMemory, WaitingProcess waitingProcess, List<Process> processesRunning) {
        Gson gson = new Gson();
        this.clock = clock;
        this.numberProcess = numberProcess;
        this.freeMemory = gson.fromJson(gson.toJson(freeMemory), FreeMemory.class);
        this.waitingProcess = gson.fromJson(gson.toJson(waitingProcess), WaitingProcess.class);
        this.processesRunning = gson.fromJson(gson.toJson(processesRunning), new TypeToken<ArrayList<Process>>() {
        }.getType());
        freeMemory.getBlocksMemory().forEach(block -> freeMemorySize += block.getTotalSize());
        this.memoryBlocks = getMemoryBlocks();
        buildColors();
    }

    public List<Number> getMemoryValuePartitions() {
        List<Number> list = memoryBlocks.stream()
                .map(Block::getTotalSize)
                .collect(Collectors.toList());
        list.add(0, Constants.OS_SIZE);
        return list;
    }

    public List<String> getMemoryNamePartitions() {
        List<String> list = memoryBlocks.stream()
                .map(Block::getName)
                .collect(Collectors.toList());
        list.add(0, Constants.OS_NAME);
        return list;
    }

    private List<Block> getMemoryBlocks() {
        List<Block> processBlocks = processesRunning.stream().map(Process::getBlock).collect(Collectors.toList());
        List<Block> freeBlocks = freeMemory.getBlocksMemory();
        processBlocks.addAll(freeBlocks);
        return processBlocks.stream()
                .sorted(Comparator.comparing(Block::getStartLocation)).collect(Collectors.toList());
    }

    public List<Color> getBlocksColors(List<String> names) {
        List<Color> colors = new ArrayList<>();
        names.forEach(name -> {
            if (name.equals(Constants.FREE_BLOCK_NAME)) {
                colors.add(Constants.FREE_MEMORY_COLOR);
            } else if (name.equals(Constants.OS_NAME)) {
                colors.add(Constants.OS_COLOR);
            } else {
                colors.add(processColors.get(name));
            }
        });
        return colors;
    }

    private void buildColors() {
        processesRunning.forEach(process -> {
            if (!processColors.containsKey(process.getName())) {
                int index = RandomUtil.generateRandomNumber(0, availableColors.size() - 1);
                processColors.put(process.getName(), availableColors.get(index));
            }
        });
        availableColors = new ArrayList<>(Constants.colors.values());
        processColors.values().forEach(processColor -> availableColors.remove(processColor));
    }

}
