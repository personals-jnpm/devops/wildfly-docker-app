package com.usta.mainmemorymanager.models;

import com.usta.mainmemorymanager.utils.Constants;
import lombok.Data;

@Data
public class Block {

    private int startLocation;
    private int endLocation;
    private int totalSize;

    private String name;

    public Block(int startLocation, int endLocation) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.name = Constants.FREE_BLOCK_NAME;
        calculateTotalSize();
    }

    public Block(int startLocation, int endLocation, String name) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.name = name;
        calculateTotalSize();
    }

    public void setStartLocation(int startLocation) {
        this.startLocation = startLocation;
        calculateTotalSize();
    }

    public void setEndLocation(int endLocation) {
        this.endLocation = endLocation;
        calculateTotalSize();
    }

    public void calculateTotalSize() {
        this.totalSize = this.endLocation - this.startLocation;
    }
}
