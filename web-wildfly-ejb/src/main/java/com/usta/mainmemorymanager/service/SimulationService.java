package com.usta.mainmemorymanager.service;

import com.usta.mainmemorymanager.enums.ProcessState;
import com.usta.mainmemorymanager.models.Process;
import com.usta.mainmemorymanager.models.*;
import com.usta.mainmemorymanager.utils.RandomUtil;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.usta.mainmemorymanager.utils.Constants.MAX_PROCESS_TO_GENERATE;
import static com.usta.mainmemorymanager.utils.Constants.PROCESS_NAME;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class SimulationService {

    private int clock;
    private int numberProcess;
    private FreeMemory freeMemory;
    private WaitingProcess waitingProcess;
    private List<Process> processesRunning;

    public List<InstantMemory> startSimulation(int numberClockCycles, int sizeMemory) {
        List<InstantMemory> instantMemories = new ArrayList<>();
        waitingProcess = new WaitingProcess();
        processesRunning = new ArrayList<>();
        freeMemory = new FreeMemory(sizeMemory);
        clock = 0;
        numberProcess = 1;
        while (clock < numberClockCycles || processesRunning.size() > 0) {
            clock++;
            inspectRunningProcess();
            inspectQueuedProcess();
            generateProcess(numberClockCycles);
            instantMemories.add(new InstantMemory(clock, numberProcess, freeMemory, waitingProcess, processesRunning));
        }
        return instantMemories;
    }

    private void generateProcess(int numberClockCycles) {
        List<Process> processesGenerated = generateRandomProcess(numberClockCycles);
        if (processesGenerated.size() > 0) {
            validateProcessSpace(processesGenerated);
        }
    }

    private void inspectRunningProcess() {
        processesRunning.forEach(process -> {
            if (process.inspectProcess()) {
                freeMemory.addFreeBlockMemory(new Block(process.getStartLocation(), process.getEndLocation()));
                process.finish(clock);
            }
        });
        processesRunning = processesRunning.stream()
                .filter(process -> process.getState() != ProcessState.FINISHED).collect(Collectors.toList());
    }

    private void inspectQueuedProcess() {
        if (waitingProcess.getQueuedProcesses().size() > 0) {
            List<Block> blocksAvailable = getAvailableFreeMemoryBlocks(waitingProcess.getQueuedProcesses().peek());
            if (blocksAvailable.size() > 0) {
                addProcessToMainMemory(blocksAvailable.get(0), Objects.requireNonNull(waitingProcess.getQueuedProcesses().poll()));
            }
        }
    }

    private void validateProcessSpace(List<Process> processes) {
        for (Process process : processes) {
            List<Block> blocksAvailable = getAvailableFreeMemoryBlocks(process);
            if (blocksAvailable.size() > 0) {
                addProcessToMainMemory(blocksAvailable.get(0), process);
            } else {
                waitingProcess.addProcessToQueue(process);
            }
        }
    }

    private List<Block> getAvailableFreeMemoryBlocks(Process process) {
        return freeMemory.getSortedBlocksBySize().stream()
                .filter(block -> block.getTotalSize() >= process.getSize())
                .collect(Collectors.toList());
    }

    private void addProcessToMainMemory(Block blocksAvailable, Process process) {
        process.start(blocksAvailable.getStartLocation(), clock);
        blocksAvailable.setStartLocation(process.getEndLocation());
        blocksAvailable.calculateTotalSize();
        processesRunning.add(process);
    }

    private List<Process> generateRandomProcess(int numberClockCycles) {
        List<Process> processesGenerated = new ArrayList<>();
        if (clock < numberClockCycles) {
            for (int i = 0; i < RandomUtil.generateRandomNumber(0, MAX_PROCESS_TO_GENERATE); i++) {
                processesGenerated.add(new Process(String.format(PROCESS_NAME, numberProcess)));
                numberProcess++;
            }
        }
        return processesGenerated;
    }

}
