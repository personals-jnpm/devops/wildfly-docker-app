FROM maven:3.6.1-jdk-8-alpine AS build
RUN mkdir -p /workspace
WORKDIR /workspace

COPY pom.xml .
COPY web-wildfly-ejb /workspace/web-wildfly-ejb
COPY web-wildfly-web /workspace/web-wildfly-web
COPY web-wildfly-ear /workspace/web-wildfly-ear
RUN mvn -f pom.xml clean package

FROM jboss/wildfly:latest
RUN /opt/jboss/wildfly/bin/add-user.sh admin-wf 123456 --silent
# ADD customization/standalone.xml /opt/jboss/wildfly/standalone/configuration/
COPY --from=build /workspace/web-wildfly-ear/target/web-wildfly-ear-1.0-SNAPSHOT.ear /opt/jboss/wildfly/standalone/deployments/
EXPOSE 80 9990 8009 $PORT
CMD /opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 -c standalone.xml -Djboss.http.port=$PORT